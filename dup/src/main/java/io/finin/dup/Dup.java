package io.finin.dup;
import java.io.InputStream;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Dup {
  
  private static final boolean                 DRY           = false;
  private static final boolean                 DEL_FULL      = false;
  private static final boolean                 DEL_WHP       = false;
  private static final boolean                 DEL_POTENTIAL = false;
  
  private static final int                     MO            = 1024 * 1024;
  
  private static final Map<String, List<Path>> REF           = new HashMap<>();
  private static final Map<String, List<Path>> DUP           = new HashMap<>();
  
  private static final Path                    REF_ROOT      = Paths.get("C:/Utilisateurs/A208220/Documents/SpiderOak Hive");
  private static final Path                    DUP_ROOT      = Paths.get("C:/Utilisateurs/A208220/Documents/SpiderOak Hive");
  // private static final Path REF_ROOT = Paths.get("D:/test/ref");
  // private static final Path DUP_ROOT = Paths.get("D:/test/dup");
  private static ExecutorService               REF_MAKER     = Executors.newFixedThreadPool(4);
  
  public static void main(String[] args) throws Exception {
    
    
    
    if (Files.isSameFile(REF_ROOT, DUP_ROOT)) {
      System.out.println("one way");
      oneWay();
    } else {
      System.out.println("two ways");
      twoWays();
    }
    
  }
  
  private static void oneWay() throws Exception {
    {
      System.out.println("Begin scanning");
      
      makeRef(REF_ROOT, REF_ROOT, REF);
      
      REF_MAKER.shutdown();
      
      while (!REF_MAKER.isTerminated()) {
        REF_MAKER.awaitTermination(10, TimeUnit.SECONDS);
      }
      
      System.out.println("Terminated scanning");
    }
    
    String key;
    List<Path> entry;
    
    for (Map.Entry<String, List<Path>> ref : REF.entrySet()) {
      key = ref.getKey();
      entry = ref.getValue();
      Collections.sort(entry);
      
      if (entry.size() == 1) continue;
      
      System.out.println("### " + key);
      for (Path p : entry) {
        System.out.println("#rm \"" + REF_ROOT.resolve(p) + "\"");
      }
    }
  }
  
  private static void twoWays() throws Exception {
    {
      System.out.println("Begin scanning");
      
      makeRef(REF_ROOT, REF_ROOT, REF);
      makeRef(DUP_ROOT, DUP_ROOT, DUP);
      
      REF_MAKER.shutdown();
      
      while (!REF_MAKER.isTerminated()) {
        REF_MAKER.awaitTermination(10, TimeUnit.SECONDS);
      }
      
      System.out.println("Terminated scanning");
    }
    
    String key;
    List<Path> entry;
    List<Path> candidates;
    
    for (Map.Entry<String, List<Path>> dup : DUP.entrySet()) {
      key = dup.getKey();
      entry = dup.getValue();
      
      candidates = REF.get(key);
      
      if (null == candidates) continue;
      
      boolean delete = false;
      for (Path p : entry) {
        delete = false;
        if (candidates.contains(p)) {
          // System.out.println("# full dup found: ");
          System.out.println("# [FULL]     " + REF_ROOT.resolve(p));
          delete = DEL_FULL;
        } else {
          int _whp = 0;
          for (Path c : candidates) {
            if (p.getFileName().equals(c.getFileName())) {
              _whp++;
            }
          }
          if (_whp > 0) {
            for (Path a : candidates) {
              System.out.println("# [WHP]      " + REF_ROOT.resolve(a));
            }
            delete = DEL_WHP;
          }
          if (_whp == 0) {
            for (Path a : candidates) {
              System.out.println("# [POTENTIAL]" + REF_ROOT.resolve(a));
            }
            delete = DEL_POTENTIAL;
          }
        }
        if (delete) {
          System.out.println("# deleting " + DUP_ROOT.resolve(p));
          if (!DRY && !DUP_ROOT.resolve(p).toFile().delete()) {
            System.out.println("rm \"" + DUP_ROOT.resolve(p) + "\"");
          }
        } else {
          System.out.println("#rm \"" + DUP_ROOT.resolve(p) + "\"");
        }
      }
    }
  }
  
  // ///////////////////////////////////
  // ///////////////////////////////////
  // /// REF MAKER
  // ///////////////////////////////////
  // ///////////////////////////////////
  
  private static void makeRef(Path path, final Path root, final Map<String, List<Path>> ref) throws Exception {
    try (DirectoryStream<Path> ds = Files.newDirectoryStream(path)) {
      for (Path entry : ds)
        if (Files.isDirectory(entry)) {
          makeRef(entry, root, ref);
        } else if (Files.isReadable(entry)) {
          final Path e = entry;
          REF_MAKER.execute(new Runnable() {
            @Override
            public void run() {
              processRef(e, root, ref);
            }
          });
        } else {
          System.out.println("Cannot process or visit '" + entry + "'");
        }
    }
  }
  
  private static void processRef(Path entry, Path root, Map<String, List<Path>> ref) {
    try (InputStream is = Files.newInputStream(entry, StandardOpenOption.READ)) {
      MessageDigest md = MessageDigest.getInstance("SHA1");
      DigestInputStream dis = new DigestInputStream(is, md);
      byte[] b = new byte[MO];
      while (-1 != dis.read(b));
      String digest = toHexa(md.digest());
      Path rel = root.relativize(entry);
      addRef(digest, rel, ref);
      // System.out.println("[REF]: " + digest + "\t" + entry);
    } catch (Throwable t) {
      System.err.println("Could not process '" + entry + "': " + t.getMessage());
    }
  }
  
  private static void addRef(String digest, Path path, Map<String, List<Path>> ref) {
    synchronized (ref) {
      List<Path> list = ref.get(digest);
      if (null == list) {
        list = new ArrayList<>();
        ref.put(digest, list);
      }
      list.add(path);
    }
  }
  
  // ///////////////////////////////////
  // ///////////////////////////////////
  // ///////////////////////////////////
  // ///////////////////////////////////
  // ///////////////////////////////////
  
  private static final char[] HEXA = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
  
  private static String toHexa(byte[] bs) {
    char[] c = new char[bs.length * 2];
    int j;
    for (int i = 0; i < bs.length; i++) {
      j = i << 1;
      c[j] = HEXA[(bs[i] & 0x0F0) >> 4];
      c[j + 1] = HEXA[bs[i] & 0x00F];
    }
    return new String(c);
  }
  
}
