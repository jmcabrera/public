package io.test.scala.map

import scala.collection.concurrent.TrieMap

import io.test.runner.api.MapCandidate

class SMM() extends MapCandidate {
  val map = collection.mutable.Map.empty[Integer, Integer]

  override def clear(): Unit = this.synchronized {
    map.clear
  }

  override def get(key: Integer): Integer = this.synchronized {
    map.getOrElse(key, -1)
  }

  override def name(): String = "Scala Mutable Map"

  override def put(key: Integer, value: Integer): Unit = this.synchronized {
    map.update(key, value)
  }

  override def size = this.synchronized {
    map.size
  }
}