package io.test.scala.map

import scala.collection.concurrent.TrieMap

import io.test.runner.api.MapCandidate

class STM() extends MapCandidate {
  val trie = TrieMap.empty[Integer, Integer]

  override def clear(): Unit = trie.clear

  override def get(key: Integer): Integer = trie.getOrElse(key, -1)

  override def name(): String = "Scala Trie Map"

  override def put(key: Integer, value: Integer): Unit = trie.put(key, value)

  override def size = trie.size
}