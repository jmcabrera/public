package io.test.scala.map

import scala.collection.concurrent.TrieMap

import io.test.runner.api.MapCandidate

class SHM() extends MapCandidate {
  var map = Map.empty[Integer, Integer]

  override def clear(): Unit = this.synchronized {
    map = Map.empty[Integer, Integer]
  }

  override def get(key: Integer): Integer = this.synchronized {
    map.getOrElse(key, -1)
  }

  override def name(): String = "Scala Map"

  override def put(key: Integer, value: Integer): Unit = this.synchronized {
    map = map.updated(key, value)
  }

  override def size = this.synchronized {
    map.size
  }
}