package io.test.runner.api;

public interface MapCandidate {
  
  void put(Integer key, Integer value);
  
  Integer get(Integer key);
  
  void clear();
  
  String name();
  
  int size();
  
}
