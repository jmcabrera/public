package io.test.java.map;

import io.test.runner.api.MapCandidate;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class CHM implements MapCandidate {
  
  private Map<Integer, Integer> map = new ConcurrentHashMap<>();
  
  @Override
  public void put(Integer key, Integer value) {
    map.put(key, value);
  }
  
  @Override
  public Integer get(Integer key) {
    return map.get(key);
  }
  
  @Override
  public void clear() {
    map.clear();
  }
  
  @Override
  public int size() {
    return map.size();
  }
  
  @Override
  public String name() {
    return "ConcurrentHashMap";
  }
  
}
