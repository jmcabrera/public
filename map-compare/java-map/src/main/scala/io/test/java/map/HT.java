package io.test.java.map;

import io.test.runner.api.MapCandidate;

import java.util.Hashtable;
import java.util.Map;

public class HT implements MapCandidate {
  
  private Map<Integer, Integer> map = new Hashtable<>();
  
  @Override
  public void put(Integer key, Integer value) {
    map.put(key, value);
  }
  
  @Override
  public Integer get(Integer key) {
    return map.get(key);
  }
  
  @Override
  public void clear() {
    map.clear();
  }
  
  @Override
  public int size() {
    return map.size();
  }
  
  @Override
  public String name() {
    return "HashTable";
  }
  
}
