package io.test.java.map;

import io.test.runner.api.MapCandidate;

import java.util.HashMap;
import java.util.Map;

public class HM implements MapCandidate {
  
  private Map<Integer, Integer> map = new HashMap<>();
  
  @Override
  public synchronized void put(Integer key, Integer value) {
    map.put(key, value);
  }
  
  @Override
  public synchronized Integer get(Integer key) {
    return map.get(key);
  }
  
  @Override
  public synchronized void clear() {
    map.clear();
  }
  
  @Override
  public synchronized int size() {
    return map.size();
  }
  
  @Override
  public String name() {
    return "Hashmap";
  }
  
}
