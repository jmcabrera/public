package io.test.ksu;

import io.test.runner.api.MapCandidate;
import edu.ksu.cis.viewer.BSTInterface;
import edu.ksu.cis.viewer.BinarySearchTree;

public class KSU implements MapCandidate {
  
  private BSTInterface map = new BinarySearchTree();
  
  @Override
  public synchronized void put(Integer key, Integer value) {
    map = map.put(key, value);
  }
  
  @Override
  public synchronized Integer get(Integer key) {
    return map.getTag(key);
  }
  
  @Override
  public synchronized void clear() {
    map = new BinarySearchTree();
  }
  
  @Override
  public String name() {
    return "BST";
  }
  
  @Override
  public int size() {
    return -1;
  }
  
}
