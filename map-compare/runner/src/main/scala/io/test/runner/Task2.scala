package io.test.runner

import scala.concurrent.{ ExecutionContext, Future }
import Function.{ tupled, untupled }

trait Task2[-A, -B, +Y] {

  type F2 = (A, B) => Y

  def map[M, N, Z](f: F2 => ((M, N) => Z)): Task2[M, N, Z]

  def flatMap[M, N, Z](f: F2 => Task2[M, N, Z]): Task2[M, N, Z]

  def foreach(f: F2 => Unit)

  def apply(a: A, b: B): Y

  private[this] val _id: (Int, (A, B) => Y) => F2 = { (_, f) => { (a, b) => f(a, b) } }

  def pre[M <: A, N <: B](pre: ((M, N) => ((A, B)))): Task2[M, N, Y] =
    this.map { f => (a, b) => f.tupled.compose(pre.tupled)((a, b)) }

  def repeat(nb: Int): Task2[A, B, Seq[Y]] =
    this.map { f => (a, b) => (1 to nb) map { _ => f(a, b) } }

  def prepeat[M <: A, N <: B](nb: Int)(pre: ((Int, M, N) => ((A, B)))): Task2[M , N, Seq[Y]] = {
    val curr0 = pre.curried
    this.map {
      f =>
        (a, b) =>
          val alt = f.tupled
          (1 to nb) map {
            i =>
              alt.compose(Function.uncurried(curr0(i)).tupled)((a, b))
          }
    }
  }

  def future(implicit ec: ExecutionContext): Task2[A, B, Future[Y]] =
    this.map { f => (a, b) => Future(f(a, b)) }
}

object Task2 {

  def apply[A, B, Y](f: ((A, B) => Y)) =
    SimpleTask2(f)

  implicit def untupled[A, B, Y](t1: Task1[(A, B), Y]): Task2[A, B, Y] =
    SimpleTask2[A, B, Y]((a, b) => t1.apply((a, b)))

  implicit def tupled[A, B, Y](t2: Task2[A, B, Y]): Task1[(A, B), Y] =
    Task1[(A, B), Y] { case (a, b) => t2.apply(a, b) }

}

case class SimpleTask2[A, B, Y](op: ((A, B) => Y)) extends Task2[A, B, Y] {

  val t1 = Task1(tupled(op))

  override def map[M, N, Z](f: F2 => ((M, N) => Z)) =
    t1.map { i => tupled(f(untupled(i))) }

  override def flatMap[M, N, Z](f: F2 => Task2[M, N, Z]) =
    t1.flatMap { x => f(untupled(x)) }

  override def foreach(f: F2 => Unit) =
    t1.foreach { x => f(untupled(x)) }

  override def apply(a: A, b: B) =
    t1.apply((a, b))

}

