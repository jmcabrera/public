package io.test.runner

import scala.concurrent.{ ExecutionContext, Future }
import Function.{ tupled, untupled }

trait Task4[-A, -B, -C, -D, +Y] {

  type F2 = (A, B, C, D) => Y

  def map[M, N, O, P, Z](f: F2 => ((M, N, O, P) => Z)): Task4[M, N, O, P, Z]

  def flatMap[M, N, O, P, Z](f: F2 => Task4[M, N, O, P, Z]): Task4[M, N, O, P, Z]

  def foreach(f: F2 => Unit)

  def apply(a: A, b: B, c: C, d: D): Y

  private[this] val _id: (Int, (A, B, C, D) => Y) => F2 = { (_, f) => { (a, b, c, d) => f(a, b, c, d) } }

  def pre[M <: A, N <: B, O <: C, P <: D](pre: ((M, N, O, P) => ((A, B, C, D)))): Task4[M, N, O, P, Y] = {
    this.map { g => (a, b, c, d) => g.tupled.compose(pre.tupled)((a, b, c, d)) }
  }

  def repeat(nb: Int): Task4[A, B, C, D, Seq[Y]] =
    this.map { f => (a, b, c, d) => (1 to nb) map { _ => f(a, b, c, d) } }

  def prepeat[M <: A, N <: B, O <: C, P <: D](nb: Int)(pre: ((Int, M, N, O, P) => ((A, B, C, D)))): Task4[M, N, O, P, Seq[Y]] = {
    val curr0 = pre.curried
    this.map {
      f =>
        (a, b, c, d) =>
          val alt = f.tupled
          (1 to nb) map {
            i =>
              alt.compose(Function.uncurried(curr0(i)).tupled)((a, b, c, d))
          }
    }
  }

  def future(implicit ec: ExecutionContext): Task4[A, B, C, D, Future[Y]] =
    this.map { f => (a, b, c, d) => Future(f(a, b, c, d)) }
}

object Task4 {

  def apply[A, B, C, D, Y](f: ((A, B, C, D) => Y)) =
    SimpleTask4(f)

  implicit def untupled[A, B, C, D, Y](t1: Task1[(A, B, C, D), Y]): Task4[A, B, C, D, Y] =
    SimpleTask4[A, B, C, D, Y]((a, b, c, d) => t1.apply((a, b, c, d)))

  implicit def tupled[A, B, C, D, Y](t2: Task4[A, B, C, D, Y]): Task1[(A, B, C, D), Y] =
    Task1[(A, B, C, D), Y] { case (a, b, c, d) => t2.apply(a, b, c, d) }

}

case class SimpleTask4[A, B, C, D, Y](op: ((A, B, C, D) => Y)) extends Task4[A, B, C, D, Y] {

  val t1 = Task1(tupled(op))

  override def map[M, N, O, P, Z](f: F2 => ((M, N, O, P) => Z)): Task4[M, N, O, P, Z] =
    t1.map { i => tupled(f(untupled(i))) }

  override def flatMap[M, N, O, P, Z](f: F2 => Task4[M, N, O, P, Z]): Task4[M, N, O, P, Z] =
    t1.flatMap { x => f(untupled(x)) }

  override def foreach(f: F2 => Unit) =
    t1.foreach { x => f(untupled(x)) }

  override def apply(a: A, b: B, c: C, d: D): Y =
    t1.apply((a, b, c, d))

}

