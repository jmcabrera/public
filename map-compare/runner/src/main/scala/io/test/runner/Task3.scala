package io.test.runner

import scala.concurrent.{ ExecutionContext, Future }
import Function.{ tupled, untupled }

trait Task3[-A, -B, -C, +Y] {

  type F2 = (A, B, C) => Y

  def map[M, N, O, Z](f: F2 => ((M, N, O) => Z)): Task3[M, N, O, Z]

  def flatMap[M, N, O, Z](f: F2 => Task3[M, N, O, Z]): Task3[M, N, O, Z]

  def foreach(f: F2 => Unit)

  def apply(a: A, b: B, c: C): Y

  def pre[M <: A, N <: B, O <: C](pre: ((M, N, O) => ((A, B, C)))): Task3[M, N, O, Y] =
    this.map { g => (a, b, c) => g.tupled.compose(pre.tupled)((a, b, c)) }

  def repeat(nb: Int): Task3[A, B, C, Seq[Y]] =
    this.map { g => (a, b, c) => (1 to nb) map { _ => g(a, b, c) } }

  def prepeat[M <: A, N <: B, O <: C](nb: Int)(pre: ((Int, M, N, O) => ((A, B, C)))): Task3[M, N, O, Seq[Y]] = {
    val curr = pre.curried
    this.map {
      g =>
        (a, b, c) =>
          val alt = g.tupled
          (1 to nb) map { i =>
            alt.compose(Function.uncurried(curr(i)).tupled)((a, b, c))
          }
    }
  }

  def future(implicit ec: ExecutionContext): Task3[A, B, C, Future[Y]] =
    this.map { f => (a, b, c) => Future(f(a, b, c)) }
}

object Task3 {

  def apply[A, B, C, Y](f: ((A, B, C) => Y)) =
    SimpleTask3(f)

  implicit def untupled[A, B, C, Y](t1: Task1[(A, B, C), Y]): Task3[A, B, C, Y] =
    SimpleTask3[A, B, C, Y]((a, b, c) => t1.apply((a, b, c)))

  implicit def tupled[A, B, C, Y](t2: Task3[A, B, C, Y]): Task1[(A, B, C), Y] =
    Task1[(A, B, C), Y] { case (a, b, c) => t2.apply(a, b, c) }

}

case class SimpleTask3[A, B, C, Y](op: ((A, B, C) => Y)) extends Task3[A, B, C, Y] {

  val t1 = Task1(tupled(op))

  override def map[M, N, O, Z](f: F2 => ((M, N, O) => Z)): Task3[M, N, O, Z] =
    t1.map { i => tupled(f(untupled(i))) }

  override def flatMap[M, N, O, Z](f: F2 => Task3[M, N, O, Z]): Task3[M, N, O, Z] =
    t1.flatMap { x => f(untupled(x)) }

  override def foreach(f: F2 => Unit) =
    t1.foreach { x => f(untupled(x)) }

  override def apply(a: A, b: B, c: C): Y =
    t1.apply((a, b, c))

}

