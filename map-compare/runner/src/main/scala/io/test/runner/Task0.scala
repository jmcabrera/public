package io.test.runner

import scala.concurrent.{ ExecutionContext, Future }

trait Task0[+O] extends Task {

  def map[P](f: O => P): Task0[P]

  def flatMap[P](f: O => Task0[P]): Task0[P]

  def foreach(f: O => Unit): Unit

  def apply(): O

  def repeat(nb: Int): Task0[Seq[O]] =
    this.map { g => (1 to nb) map { _ => g } }

  def future(implicit ec: ExecutionContext): Task0[Future[O]] =
    this.map { f => Future(f) }
}

object Task0 {

  def apply[O](f: => O): Task0[O] = new SimpleTask0(f)

  object Empty extends Task1[Any, Nothing] {
    override def map[C, O](f: (Any => Nothing) => (C => O)) = this
    override def flatMap[C, O](f: (Any => Nothing) => Task1[C, O]) = this
    override def foreach(f: (Any => Nothing) => Unit) {}
    override def apply(v: Any) = ???
  }
}

class SimpleTask0[+O](op: => O) extends Task0[O] {
  override def map[P](f: O => P): Task0[P] = Task0(f(op))

  override def flatMap[P](f: O => Task0[P]): Task0[P] = f(op)

  override def foreach(f: O => Unit): Unit = f(op)

  override def apply() = op
}

