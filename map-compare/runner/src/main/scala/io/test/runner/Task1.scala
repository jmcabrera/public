package io.test.runner

import scala.concurrent.{ ExecutionContext, Future }

trait Task1[-A, +Y] {

  type F = A => Y

  def map[M, Z](f: F => (M => Z)): Task1[M, Z]

  def flatMap[M, Z](f: F => Task1[M, Z]): Task1[M, Z]

  def foreach(f: F => Unit): Unit

  def apply(v: A): Y

  def pre[M <: A](pre: ((M) => A)): Task1[M, Y] =
    this.map { f => a => f.compose(pre)(a) }

  def repeat(nb: Int): Task1[A, Seq[Y]] =
    this.map { f => a => (1 to nb) map { _ => f(a) } }

  def prepeat[M <: A](nb: Int)(pre: ((Int, M) => A)): Task1[M, Seq[Y]] = {
    val curr = pre.curried
    this.map {
      g =>
        c =>
          (1 to nb) map { i => g.compose(curr(i))(c) }
    }
  }

  def future(implicit ec: ExecutionContext): Task1[A, Future[Y]] =
    this.map { f => a => Future { f(a) } }
}

object Task1 {

  def apply[A, Y](f: A => Y): Task1[A, Y] = new SimpleTask1(f)

  object Empty extends Task1[Any, Nothing] {
    override def map[M, Y](f: (Any => Nothing) => (M => Y)) = this
    override def flatMap[M, Y](f: (Any => Nothing) => Task1[M, Y]) = this
    override def foreach(f: (Any => Nothing) => Unit) {}
    override def apply(v: Any) = ???
  }
}

class SimpleTask1[-A, +Y](op: A => Y) extends Task1[A, Y] {
  override def map[M, N](f: (A => Y) => (M => N)) =
    Task1(f(op))

  override def flatMap[M, Z](f: (A => Y) => Task1[M, Z]): Task1[M, Z] =
    f(op)

  override def foreach(f: (A => Y) => Unit): Unit =
    f(op)

  override def apply(v: A) =
    op(v)
}

