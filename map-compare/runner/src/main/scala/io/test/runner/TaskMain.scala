package io.test.runner

import scala.concurrent._
import duration._
import java.util.concurrent.Executors

import Task._

object TaskMain extends App {

  def task(s: String) = {
    Thread.sleep(1000)
    s"Hello, $s on thread ${Thread.currentThread().getName}"
  }

  println("Creating initial task")
  val t0 = Task(task _)

  implicit val ec = ExecutionContext.fromExecutorService(Executors.newFixedThreadPool(4))

  val t1 = t0.future.prepeat(10) {
    (i, s: String) => s"iteration #$i: $s"
  }

  val fres = t1.apply("Task")

  fres.foreach {
    s =>
      s.onSuccess {
        case x => println(x)
      }
  }

  val res = Await.result(Future.sequence(fres), 1 hour)

  println("finished")

  ec.shutdown()
  //  val t3 = t1 map {
  //    f =>
  //      s: String =>
  //        println("opening a threadpool")
  //        implicit val ec = ExecutionContext.fromExecutorService(Executors.newFixedThreadPool(4))
  //        f((s, ec))
  //  }

  //  println("mapping it 1st time")
  //  println(t2("tasks"))
  //
  //  println("Multiplying it")
  //
  //  def times(nb: Int)(t: String => String): Task[String, Seq[String]] =
  //    SimpleTask1({
  //      s: String =>
  //        (1 to nb) map {
  //          i =>
  //            s"#$i:" + t(s)
  //        }
  //    })
  //
  //  println("applying it 2nd time")
  //  t2.repeat(10)("Tasks 2") foreach println
  //
  //  println("applying it 3nd time")
  //  t2.repeat(10)({ (i: Int, f: String => Unit) => s: String => s"#$i:" + f(s) })("Tasks 2") foreach println

}
