package io.test.runner

trait Task {
//  def before(b: => Unit): this.type
//  def after(a: => Unit): this.type
}

object Task {
  def apply[y](f: => y) = Task0(f)
  def apply[a, y](f: a => y) = Task1(f)
  def apply[a, b, y](f: (a, b) => y) = Task2(f)
  def apply[a, b, c, y](f: (a, b, c) => y) = Task3(f)
  def apply[a, b, c, d, y](f: (a, b, c, d) => y) = Task4(f)
  def apply[a, b, c, d, e, y](f: (a, b, c, d, e) => y) = Task5(f)
}
