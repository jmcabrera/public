package io.test.runner

import java.util.Iterator
import java.util.ServiceLoader
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import scala.annotation.tailrec
import scala.collection.JavaConversions._
import scala.concurrent._
import scala.util.Random._
import io.test.runner.api.MapCandidate
import scala.concurrent.Future
import scala.concurrent.duration._
import java.util.concurrent.TimeUnit
import java.util.concurrent.CountDownLatch
import java.io.PrintWriter
import java.io.FileWriter
import Math._

object MainRunner extends App {

  /* 
   * STATICS 
   */
  setSeed(0)

  val TEN = 10
  val HUNDRED = TEN * TEN
  val THOUSAND = HUNDRED * TEN
  val MILLION = THOUSAND * THOUSAND

  val replays = TEN
  val NB_THREADS = 1 to 16

  /*
   * CONFS 
   */

  val resetBeforeReplay: MapCandidate => MapCandidate = { i => i.clear; i }

  val resetBeforeOperation: MapCandidate => MapCandidate = { i => i }

  val operations: Seq[Operation] = Seq(
    Operation("put", { (map: MapCandidate, v: Integer) => map.put(v, v) }),
    Operation("get", { (map: MapCandidate, v: Integer) => map.get(v); {} }))

  /* 
   * Locating instances 
   */
  val sl: Iterator[MapCandidate] = ServiceLoader.load(classOf[MapCandidate]).iterator

  val instances = Seq(Dumb) ++ (for {
    i <- sl
  } yield {
    i
  }).toList

  /*
   * inputs
   */
  println("calculating input data")
  val inputs = (Seq( /*HUNDRED, THOUSAND, */ TEN * THOUSAND, HUNDRED * THOUSAND /*, MILLION*/ ) map { Array.fill(_)(nextInt) }).toArray

  {
    println("warmup")
    implicit val ec = open(4)
    runTask(4)
    close
  }

  println("GO!")
  val samples = for {
    nbThreads <- NB_THREADS
  } yield {
    println(s"running $nbThreads")
    implicit val ec = open(nbThreads)
    val s = runTask(nbThreads)
    close
    s
  }

  println("Doing stats")
  //  val stats = new PrintWriter(new FileWriter("stats.csv"));

  samples.flatten foreach { s =>
    import s._
    println(s"""$nbThreads "$instanceName" "$operation" $nbInserts $timeSpent $nbNorm $timeSpentNorm""")
  }

  //  stats.close
  println("Done")

  def runTask(nbThreads: Int)(implicit es: ExecutionContextExecutorService) = {

    val round = (Task {
      (in: Array[Int], from: Int, to: Int, instance: MapCandidate, op: Operation) =>
        var tot = 0d
        for {
          j <- from until to
        } {
          tot = tot + time(op.op(instance, in(j)))
        }
        tot
    }).future

    val oneInstance = Task {
      (in: Array[Int], nbThreads: Int, instance: MapCandidate) =>
        val factor = in.length / nbThreads
        val realSize = factor * nbThreads
        for {
          op <- operations
        } yield {
          val opsres = for {
            i <- 0 until nbThreads
            from = i * factor
            to = (i + 1) * factor
          } yield {
            round(in, from, to, instance, op)
          }
          val res = Await.result(Future.sequence(opsres), 1 hour)
          Sample(nbThreads, op.name, instance.name, realSize, res.sum)
        }
    }.pre {
      (a: Array[Int], b: Int, c: MapCandidate) => resetBeforeReplay(c); (a, b, c)
    }.repeat(replays)

    val main = Task {
      for {
        in <- inputs
        map <- instances
      } yield {
        oneInstance(in, nbThreads, map)
      }
    }

    main().flatten.flatten
  }

  /**
   * Run all tests on all inputs for all candidates, for the given number of threads
   */
  def run(nbThreads: Int)(implicit e: ExecutionContextExecutorService) = for {
    in <- inputs
    factor = in.length / nbThreads
    realSize = factor * nbThreads
    instance <- instances
    replay <- 1 to replays
    _ = resetBeforeReplay(instance)
    op <- operations
  } yield {
    println(s"Running ${instance.name}.${op.name} with $realSize on $nbThreads (replay #$replay)")

    resetBeforeOperation(instance)

    val ops = for {
      i <- 0 until nbThreads
      from = i * factor
      to = (i + 1) * factor
    } yield future {
      var tot = 0d
      for {
        j <- from until to
        v = in(j)
      } {
        tot = tot + time(op.op(instance, v))
      }
      tot
    }
    val opsres = Await.result(Future.reduce(ops)(_ + _), 1 hour)

    val s = Sample(nbThreads, op.name, instance.name, realSize, opsres)
    import s._
    println(s"""${s.nbThreads} "$instanceName" "$operation" $nbInserts $timeSpent $nbNorm $timeSpentNorm""")
    s
  }

  def open(nb: Int): ExecutionContextExecutorService = {
    ExecutionContext.fromExecutorService(Executors.newFixedThreadPool(nb))
  }

  def close(implicit ec: ExecutionContextExecutorService) {
    val list = ec.shutdownNow()
    if (!list.isEmpty) {
      println(s"""//!\\ ${list.size} jobs still in the pipe //!\\""")
    }
    // cool down...
    System.gc
    Thread.sleep(100)
  }

  def time[A](f: => A): (Double, A) = {
    val beg = System.nanoTime
    val res = f
    val end = System.nanoTime
    ((end - beg) / 1000000.0, res)
  }

  def time(f: => Unit): Double = {
    val beg = System.nanoTime
    f
    val end = System.nanoTime
    (end - beg) / 1000000.0
  }

  def pad(i: Int)(str: String) = s"%1$$${i}s" format str

}

case class Operation(name: String, op: (MapCandidate, Integer) => Unit)

case class Sample(nbThreads: Int, operation: String, instanceName: String, nbInserts: Int, timeSpent: Double) {
  val nbNorm = nextPowOf10(nbInserts)
  val timeSpentNorm = timeSpent / nbInserts * nbNorm
  def nextPowOf10(i: Int) = pow(10, ceil(log10(i)))
}

case object Dumb extends MapCandidate {

  override def clear() {}

  override def get(key: Integer) = key

  override def name = "Dumb"

  override def put(key: Integer, value: Integer) {}

  override def size = 0
}
