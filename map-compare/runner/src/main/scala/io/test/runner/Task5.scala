package io.test.runner

import scala.concurrent.{ ExecutionContext, Future }
import Function.{ tupled, untupled }

trait Task5[-A, -B, -C, -D, -E, +Y] {

  type F = (A, B, C, D, E) => Y

  def map[M, N, O, P, Q, Z](f: F => ((M, N, O, P, Q) => Z)): Task5[M, N, O, P, Q, Z]

  def flatMap[M, N, O, P, Q, Z](f: F => Task5[M, N, O, P, Q, Z]): Task5[M, N, O, P, Q, Z]

  def foreach(f: F => Unit)

  def apply(a: A, b: B, c: C, d: D, e: E): Y

  private[this] val _id: (Int, (A, B, C, D, E) => Y) => F = { (_, f) => { (a, b, c, d, e) => f(a, b, c, d, e) } }

  def pre[M <: A, N <: B, O <: C, P <: D, Q <: E](pre: ((M, N, O, P, Q) => ((A, B, C, D, E)))): Task5[M, N, O, P, Q, Y] = {
    this.map { g => (a, b, c, d, e) => g.tupled.compose(pre.tupled)((a, b, c, d, e)) }
  }

  def repeat(nb: Int): Task5[A, B, C, D, E, Seq[Y]] =
    this.map { f => (a, b, c, d, e) => (1 to nb) map { _ => f(a, b, c, d, e) } }

  def prepeat[M <: A, N <: B, O <: C, P <: D, Q <: E](nb: Int)(pre: ((Int, M, N, O, P, Q) => ((A, B, C, D, E)))): Task5[M, N, O, P, Q, Seq[Y]] = {
    val curr0 = pre.curried
    this.map {
      f =>
        (a, b, c, d, e) =>
          val alt = f.tupled
          (1 to nb) map {
            i =>
              alt.compose(Function.uncurried(curr0(i)).tupled)((a, b, c, d, e))
          }
    }
  }

  def future(implicit ec: ExecutionContext): Task5[A, B, C, D, E, Future[Y]] =
    this.map { f => (a, b, c, d, e) => Future(f(a, b, c, d, e)) }
}

object Task5 {

  def apply[A, B, C, D, E, Y](f: ((A, B, C, D, E) => Y)) =
    SimpleTask5(f)

  implicit def untupled[A, B, C, D, E, Y](t1: Task1[(A, B, C, D, E), Y]): Task5[A, B, C, D, E, Y] =
    SimpleTask5[A, B, C, D, E, Y]((a, b, c, d, e) => t1.apply((a, b, c, d, e)))

  implicit def tupled[A, B, C, D, E, Y](t2: Task5[A, B, C, D, E, Y]): Task1[(A, B, C, D, E), Y] =
    Task1[(A, B, C, D, E), Y] { case (a, b, c, d, e) => t2.apply(a, b, c, d, e) }

}

case class SimpleTask5[A, B, C, D, E, Y](op: ((A, B, C, D, E) => Y)) extends Task5[A, B, C, D, E, Y] {

  val t1 = Task1(tupled(op))

  override def map[M, N, O, P, Q, Z](f: F => ((M, N, O, P, Q) => Z)): Task5[M, N, O, P, Q, Z] =
    t1.map { i => tupled(f(untupled(i))) }

  override def flatMap[M, N, O, P, Q, Z](f: F => Task5[M, N, O, P, Q, Z]): Task5[M, N, O, P, Q, Z] =
    t1.flatMap { x => f(untupled(x)) }

  override def foreach(f: F => Unit) =
    t1.foreach { x => f(untupled(x)) }

  override def apply(a: A, b: B, c: C, d: D, e: E): Y =
    t1.apply((a, b, c, d, e))

}

