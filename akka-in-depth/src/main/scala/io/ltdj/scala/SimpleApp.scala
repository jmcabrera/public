package io.ltdj.scala

// simple interface
trait SimpleTrait {
  def print
}

// simple class
class SimpleClass(a: String, b: String) extends SimpleTrait {

  println("Hello, I am a simple class")

  override def print = println(a + " " + b)
}

class AnotherSimpleClass(a: String, b: String, c: String) extends SimpleClass(a, b) {

  println("Hello, I am another simple class")

  override def print = println(a + " " + b + " " + c)
}

// Companion object
object AnotherSimpleClass {
  def apply(c: String) = new AnotherSimpleClass("", "", c)
}

// simple main
object SimpleApp extends App {

  println("#### Simple class")
  val sc = new SimpleClass("hello", "Scala")
  sc.print

  println("#### Another Simple class")
  val asc = new AnotherSimpleClass("hello", "Scala", "Again")
  asc.print

  println("#### Another Simple class with companion")
  val oasc = AnotherSimpleClass("Simple message")
  oasc.print

}