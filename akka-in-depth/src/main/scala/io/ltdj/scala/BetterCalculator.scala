package io.ltdj.scala

import scala.util.Try
import scala.util.Success
import scala.util.Failure

object Op {
  implicit def intToVal(i: Int) = Val(i)

}
sealed trait Op {
  def run: Try[Int]
}

case class Val(val value: Int) extends Op {
  def run = Success(value)
}

case class Add(left: Op, right: Op) extends Op {
  def run =
    for {
      l <- left.run
      r <- right.run
    } yield l + r
}

case class Opposite(value: Op) extends Op {
  def run =
    for {
      v <- value.run
    } yield -v
}

case class Mult(left: Op, right: Op) extends Op {
  def run =
    for {
      l <- left.run
      r <- right.run
    } yield l * r
}

case class Inverse(value: Op) extends Op {
  def run = for {
    v <- value.run
  } yield 1 / v
}

object BetterCalculator extends App {

  val v1 = Add(Mult(1, 2), Mult(3, 4))
  val v2 = Inverse(0)

  println(v1.run)

  println(v2.run)

}