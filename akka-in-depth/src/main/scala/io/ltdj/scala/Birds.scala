package io.ltdj.scala

import scala.collection.mutable.ListBuffer

trait Animal {
  def capabilities = List("I can move")
  def attributes = List("I have guts")
}

trait Swims extends Animal {
  override def capabilities = super.capabilities :+ "I can swim"
  override def attributes = super.attributes :+ "I have webbed feets"
}

trait Flies extends Animal {
  override def capabilities = super.capabilities :+ "I can fly"
}

trait MakeEggs extends Animal {
  override def capabilities = super.capabilities :+ "I can make eggs"
}

trait HasFeathers extends Animal {
  override def attributes = super.attributes :+ "I have feathers"
}

trait HasPeak extends Animal {
  override def attributes = super.attributes :+ "I have a peak"
}

trait Bird extends Animal
  with MakeEggs
  with HasFeathers
  with HasPeak

class Rapace extends Bird with Flies

object Birds extends App {

  // Mixin
  object penguin extends Bird with Swims with Flies

  for (capability <- penguin capabilities) {
    println(capability)
  }

  for (attribute <- penguin attributes) {
    println(attribute)
  }

}