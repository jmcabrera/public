package io.ltdj.scala;

interface JavaSimpleTrait {
  public void print();
}

class JavaSimpleClass implements JavaSimpleTrait {
  public final String a;
  
  public final String b;
  
  public JavaSimpleClass(String a, String b) {
    this.a = a;
    this.b = b;
    System.out.println("Hello, I am a simple class");
  }
  
  @Override
  public void print() {
    System.out.println(a + " " + b);
  }
  
}

class JavaAnotherSimpleClass extends JavaSimpleClass {
  
  public final String c;
  
  public JavaAnotherSimpleClass(String a, String b, String c) {
    super(a, b);
    this.c = c;
    System.out.println("Hello, I am another simple class");
  }
  
  public JavaAnotherSimpleClass(String c) {
    this("", "", c);
  }
  
  @Override
  public void print() {
    System.out.println(a + " " + b + " " + c);
  }
}

public class JavaSimpleApp {
  
  public static void main(String[] args) {
    System.out.println("#### Simple class");
    JavaSimpleClass sc = new JavaSimpleClass("hello", "Scala");
    sc.print();
    
    System.out.println("#### Another Simple class");
    JavaAnotherSimpleClass asc = new JavaAnotherSimpleClass("hello", "Scala", "Again");
    asc.print();
    
    System.out.println("#### Another Simple class with companion");
    JavaAnotherSimpleClass oasc = new JavaAnotherSimpleClass("Simple message");
    oasc.print();
  }
}