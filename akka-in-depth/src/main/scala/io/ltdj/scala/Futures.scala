package io.ltdj.scala

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{ Success, Failure }

object Futures extends App {

  val fa = Future {
    Thread.sleep(1500)
    println("a is ready")
    "a"
  }
  val fb = Future {
    Thread.sleep(1700)
    println("b is ready")
    "b"
  }
  val ff = Future {
    Thread.sleep(200)
    println("f is ready")
    throw new Exception
    "f"
  }

  val fc = for {
    a <- fa
    b <- fb
    f <- ff
  } yield {
    println("c is ready")
    a + ":" + b + ":" + f
  }

  fc.onComplete {
    case Success(s) => println(s)
    case Failure(t) => println(s"Something went wrong: $t")
  }

  println("done")

  Thread.sleep(2000)

}