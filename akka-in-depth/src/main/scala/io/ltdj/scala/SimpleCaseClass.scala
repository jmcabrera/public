package io.ltdj.scala

case class Simple(a: String, b: String)

object SimpleCaseClass extends App {

  val v = Simple("Hello", "case classes")

  //println(v)

  val w = Simple("Hello", "case classes")

  //println(w == v)

  val x = v.copy(a = "Salut")

   println(x)

}