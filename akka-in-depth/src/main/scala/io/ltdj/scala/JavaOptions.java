package io.ltdj.scala;

public class JavaOptions {
  
  public static void main(String[] args) {
    
    System.out.println(work(10d));
  }
  
  public static Double work(Double d) {
    
    Double a = times5(10d);
    
    if (null != a) {
      
      Double b = plus2(a);
      
      if (null != b) {
        return a * b;
      } else {
        return null;
      }
      
    } else {
      return null;
    }
  }
  
  public static Double times5(Double d) {
    
    return d * 5;
  }
  
  public static Double plus2(Double d) {
    return d + 2;
  }
}