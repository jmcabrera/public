package io.ltdj.scala

object Tries extends App {

  object Try {
    def apply[A](f: => A): Try[A] =
      try {
        Success(f)
      } catch {
        case t: Throwable => Failure(t)
      }
  }
  sealed trait Try[+A] {
    def flatMap[B](f: A => Try[B]): Try[B]
    def map[B](f: A => B): Try[B]
  }

  case class Success[A](val a: A) extends Try[A] {
    def flatMap[B](f: A => Try[B]): Try[B] = f(a)
    def map[B](f: A => B): Try[B] = Try(f(a))
  }

  case class Failure[T <: Throwable](val t: T) extends Try[Nothing] {
    def flatMap[B](f: Nothing => Try[B]): Try[B] = this
    def map[B](f: Nothing => B): Try[B] = this
  }

  dumbTry
  simpleTry
  complexTry
  complexTry2

  def dumbTry {
    val oa = Success(1)
    val ob = Success(2)

    val oc = for {
      a <- oa
      b <- ob
    } yield a * b

    println(oc)
  }

  def simpleTry {
    val oa: Try[Int] = Success(1)
    val ob: Try[Int] = Failure(new Exception("Wont do this!"))

    val oc = for {
      a <- oa
      b <- ob
    } yield a * b

    println(oc)
  }

  def complexTry {

    def invert(x: Double): Try[Double] = Try { 1 / x }

    def increment(x: Double) = Try { x + 1 }

    val tc = for {
      a <- invert(10)
      b <- increment(a)
    } yield b

    println(tc)

    // Desugaring the for-comprehension
    val tc2 = invert(10) flatMap {
      a =>
        increment(a) map {
          b => a * b
        }
    }

    // Definition of invert
    val tc3 = Try { 1 / 10.0 } flatMap {
      a => increment(a) map { b => a * b }
    }

    // Execution of the try
    val tc4 = Success(0.10) flatMap {
      a => increment(a) map { b => a * b }
    }

    // Definition of flatMap on Success
    // Success(a).flatMap(f) = f(a) 
    val tc5 = increment(0.10) map { b => 0.10 * b }

    // Definition of increment
    val tc6 = Try { 1 + 0.10 } map { b => 0.10 * b }

    // Execution of the try
    val tc7 = Success(1.10) map { b => 0.10 * b }

    // Definition of map
    // Success(a).map(f) = f(a)
    val tc8 = Try { 0.10 * 1.10 }

    // Execution of the try
    val tc9 = Success(0.11)
  }

  def complexTry2 {

    def invert(x: Double): Try[Double] = Try { 1 / x }

    def increment(x: Double) = Try { x + 1 }

    val tc = for {
      a <- invert(0)
      b <- increment(a)
    } yield a * b

    println(tc)

    // Desugaring the for-comprehension
    val tc2 = invert(0) flatMap {
      a =>
        increment(a) map {
          b => a * b
        }
    }

    // Definition of invert
    val tc3 = Try { 1 / 0 } flatMap {
      a =>
        increment(a) map {
          b => a * b
        }
    }

    // Execution of the try
    val tc4 = Failure(new ArithmeticException()) flatMap {
      a: Double => increment(a) map { b => a * b }
    }

    // Definition of flatMap on failure:
    // Failure.flatMap = this
    val tc5 = Failure(new ArithmeticException())
  }
}