package io.ltdj.scala

object Functions extends App {

  val inc = { x: Int => x + 1 }

  val dec = { x: Int => x - 1 }

  val noop = inc andThen dec

  val doubleDec = dec andThen dec

  val doubleInc = inc compose inc

  println(noop(0))

  println(doubleDec(0))

  println(doubleInc(0))

  val intToString = { x: Int => "" + x }

  val stringToInt = { s: String => s.length }

  val sizeOfInt = stringToInt compose intToString

  val sizeOfString = intToString compose stringToInt

  println(sizeOfInt(111111))

  println(sizeOfString("abc"))
}