package io.ltdj.scala

object Lists extends App {

  // Some constants to play with
  val la = List(1, 2, 3)
  val lb = List(4, 5, 6)

  map
  filter
  flatMap
  forComprehension

  // MAP
  def map {
    println("###### MAP")
    // List[A] map[B] (f: A => B } : List[B]
    val lc = la map { x => x * x }
    println(lc)
  }

  // FILTER
  def filter {
    // List[A] filter (f: A => Boolean } : List[A]
    println("###### FILTER")
    val lc = la map { _ - 2 } filter { _ > 0 }
    println(lc)
  }

  def flatMap {
    // List[A] flatMap[B] (f: A => List[B] } : List[B]
    println("###### FLATMAP")
    val lc = la flatMap { x => List(x, x) }
    println(lc)
  }

  // FOR comprehension
  def forComprehension {
    println("###### FOR-COMPREHENSION")
    val lc = for {
      a <- la if a % 2 == 1
      b <- lb if b % 2 == 0
    } yield {
      a * b
    }

    println(lc)

    // Equivalent to:
    val lc2 =
      la filter {
        _ % 2 == 1
      } flatMap {
        a =>
          lb filter {
            b => b % 2 == 0
          } map {
            b => a * b
          }
      }

    println(lc2)
  }
}
