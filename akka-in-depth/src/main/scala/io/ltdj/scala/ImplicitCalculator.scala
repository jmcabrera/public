package io.ltdj.scala

object ImplicitCalculator extends App {

  implicit def intToValue(i: Int): Value = Value(i)

  val expression = Addition(Multiplication(1, 2), Multiplication(3, 4))

  println(expression.run)
}