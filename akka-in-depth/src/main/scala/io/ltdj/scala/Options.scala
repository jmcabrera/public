package io.ltdj.scala

object Options extends App {

  object Option {
    def apply[A](a: A) = if (null == a) None else Some(a)
  }
  sealed trait Option[+A] {
    def flatMap[B](f: A => Option[B]): Option[B]
    def map[B](f: A => B): Option[B]
  }

  case class Some[A](val a: A) extends Option[A] {
    def flatMap[B](f: A => Option[B]): Option[B] = f(a)
    def map[B](f: A => B): Option[B] = Option(f(a))
  }

  case object None extends Option[Nothing] {
    def flatMap[B](f: Nothing => Option[B]): Option[B] = None
    def map[B](f: Nothing => B): Option[B] = None
  }

  dumbOption
  simpleOption
  complexOption

  def dumbOption {
    val oa = Some(1)
    val ob = Some(2)

    val oc = for {
      a <- oa
      b <- ob
    } yield a * b

    println(oc)
  }

  def simpleOption {
    val oa = Some(1)
    val ob: Option[Int] = None

    val oc = for {
      a <- oa
      b <- ob
    } yield a * b

    println(oc)
  }

  def complexOption {
    def nothingHere(x: Int): Option[Int] = None
    def longFunction(x: Int) = Some(x + 2)

    val oc = for {
      a <- nothingHere(10)
      b <- longFunction(a)
    } yield a * b

    println(oc)

    // Desugaring the for-comprehension
    val oc2 = nothingHere(10) flatMap {
      a: Int =>
        longFunction(a) map {
          b: Int => a * b
        }
    }

    // Definition of nothingHere
    val oc3 = None flatMap {
      a: Int =>
        Some(a + 2) map {
          b: Int => a * b
        }
    }

    // Definition of flatMap on None
    // None.flatMap = None
    val oc4 = None

  }

  def complexOption2 {
    def times5(x: Int) = Some(x * 5)
    def plus2(x: Int) = Some(x + 2)

    val oc = for {
      a <- times5(10)
      b <- plus2(a)
    } yield a * b

    println(oc)

    // Desugaring the for-comprehension
    val oc2 = times5(10) flatMap {
      a: Int =>
        plus2(a) map {
          b: Int => a * b
        }
    }

    // Execution of times5
    val oc3 = Some(50) flatMap {
      a: Int =>
        plus2(a) map {
          b: Int => a * b
        }
    }

    // Definition of flatMap on Some
    // Some(a).flatMap(f) = f(a)
    val oc4 = plus2(50) map { b => 50 * b }

    // Execution of plus2
    val oc5 = Some(52) map { b => 50 * b }

    // Definition of map on Some
    // Some(a).map(f) = Option(f(a))
    val oc6 = Option(50 * 52)

    // Definition of Option.apply
    val oc7 = Some(2600)

  }
}